@extends('layouts.super-admin')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('super-admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ $pageTitle }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/switchery/dist/switchery.min.css') }}">
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">{{ $pageTitle }}</div>

                <div class="vtabs customvtab m-t-10">
                    @include('sections.super_admin_setting_menu')

                    <div class="tab-content">
                        <div id="vhome3" class="tab-pane active">
                            <div class="row">
                                <div class="col-md-12">

                                    <h3 class="box-title m-b-0">@lang("modules.stripeSettings.title")</h3>

                                    <p class="text-muted m-b-10 font-13">
                                        @lang("modules.stripeSettings.subtitle")
                                    </p>
                                            {!! Form::open(['id'=>'updateSettings','class'=>'ajax-form','method'=>'put']) !!}
                                            {!! Form::hidden('_token', csrf_token()) !!}
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>@lang("modules.stripeSettings.apiKey")</label>
                                                            <input type="text" name="api_key" id="api_key"
                                                                   class="form-control"
                                                                   value="{{ $stripeSetting->api_key }}">
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>@lang("modules.stripeSettings.apiSecret")</label>
                                                            <input type="text" name="api_secret" id="api_secret"
                                                                   class="form-control"
                                                                   value="{{ $stripeSetting->api_secret }}">
                                                        </div>
                                                    </div>
                                                    <!--/span-->

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>@lang("modules.stripeSettings.webhookKey")</label>
                                                            <input type="text" name="webhook_key" id="webhook_key"
                                                                   class="form-control"
                                                                   value="{{ $stripeSetting->webhook_key }}">
                                                                   <span class="bmd-help"> Visit <a
                                                                    href="https://dashboard.stripe.com/account/webhooks"
                                                                    target="_blank">Generate</a> Add end point as <b> {{ route('save_webhook')}}</b> and enter the webhook key generated</span>
                                                        </div>
                                                        
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                <!--/row-->
                                            </div>
                                            <div class="form-actions">
                                                <button type="submit" id="save-form" class="btn btn-success"><i
                                                            class="fa fa-check"></i>
                                                    @lang('app.update')
                                                </button>
                                                <button type="reset" class="btn btn-default">@lang('app.reset')</button>
                                            </div>
                                            {!! Form::close() !!}

                                </div>
                                <!-- .row -->

                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>
        <!-- .row -->


        @endsection

        @push('footer-script')
        <script src="{{ asset('plugins/bower_components/switchery/dist/switchery.min.js') }}"></script>
        <script>
            $('#save-form').click(function () {

                var url = '{{route('super-admin.stripe-settings.update', $stripeSetting->id)}}';

                $.easyAjax({
                    url: url,
                    type: "POST",
                    container: '#updateSettings',
                    data: $('#updateSettings').serialize()
                })
            });
        </script>
    @endpush