@extends('layouts.app')

@section('page-title')
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
                <li class="active">{{ $pageTitle }}</li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
@endsection
@push('head-script')
    <style>
        .f-15{
            font-size: 15px !important;
        }
    </style>
@endpush


@section('content')

    <div class="row">
        <div class="col-lg-12 col-sm-12">
            @if (session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @endif
            <div class="panel panel-info">
                <div class="panel-heading">Your Current Plan ({{  $company->package->name }})
                    <div class="pull-right" style="margin-top: -7px;"><a href="{{ route('admin.billing.packages') }}" class="btn btn-block btn-success waves-effect text-center">Change Plan</a> </div></div>

                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row f-15 m-b-10">
                            <div class="col-sm-9">
                                Annual Price
                            </div>
                            <div class="col-sm-3">
                                  {{ $globalSetting->currency->currency_symbol }}  {{ $company->package->annual_price }}
                            </div>
                        </div>
                        <div class="row f-15 m-b-10">
                            <div class="col-sm-9">
                                Monthly Price
                            </div>
                            <div class="col-sm-3">
                                {{ $globalSetting->currency->currency_symbol }} {{ $company->package->monthly_price }}
                            </div>
                        </div>
                        <div class="row f-15 m-b-10">
                            <div class="col-sm-9">
                                Plan's Active Employee Limit
                            </div>
                            <div class="col-sm-3">
                                {{ $company->package->max_employees }}
                            </div>
                        </div>
                        <div class="row f-15 m-b-10">
                            <div class="col-sm-9">
                                Your Current Active Employee's
                            </div>
                            <div class="col-sm-3">
                                {{ $company->employees->count() }}
                            </div>
                        </div>
                        <div class="row f-15 m-b-10">
                            <div class="col-sm-9">
                                Next Payment Date
                            </div>
                            <div class="col-sm-3">
                                {{ $nextPaymentDate }}
                            </div>
                        </div>
                        <div class="row f-15 m-b-10">
                            <div class="col-sm-9">
                                Previous Payment Date
                            </div>
                            <div class="col-sm-3">
                                {{ $previousPaymentDate }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-sm-12">
            <div class="white-box">
                <h3 class="box-title">Invoices</h3>

                <div class="table-responsive">
                    <table class="table color-table info-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Number</th>
                            <th>Package</th>
                            <th>Amount ({{ $globalSetting->currency->currency_symbol }} )</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @forelse($invoices as $key => $invoice)
                                @php
                                $date = \Carbon\Carbon::createFromTimeStamp($invoice->period_start) -> toFormattedDateString();
                                $package = \App\Package::where(function ($query) use($invoice) {
                $query->where('stripe_annual_plan_id', '=', $invoice->lines->data[0]->plan->id)
                      ->orWhere('stripe_monthly_plan_id', '=', $invoice->lines->data[0]->plan->id);
            })->first();
                                @endphp
                                <tr>
                                    <td> {{ $key+1 }} </td>
                                    <td> {{ $invoice->number }} </td>
                                    <td> {{ $package->name }} </td>
                                    <td> {{ round($invoice->total/100) }} </td>
                                    <td> {{ $date }} </td>
                                    <td> <a href="{{route('admin.stripe.invoice-download', $invoice->id)}}" class="btn btn-primary btn-circle waves-effect" data-toggle="tooltip" data-original-title="Download"><span></span> <i class="fa fa-download"></i></a>  </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5" class="text-center"> No invoice found </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('footer-script')
@endpush