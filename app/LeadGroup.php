<?php

namespace App;

use App\Observers\LeadGroupObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class LeadGroup extends Model
{
    protected $table = 'lead_groups';

    protected $guarded = ['id'];

    protected static function boot()
    {
        parent::boot();

        static::observe(LeadGroupObserver::class);

        $company = company();

        static::addGlobalScope('company', function (Builder $builder) use($company) {
            if ($company) {
                $builder->where('lead_groups.company_id', '=', $company->id);
            }
        });
    }
}
