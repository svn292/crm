<?php

namespace App\Console\Commands;

use App\Company;
use App\Currency;
use App\ModuleSetting;
use App\Package;
use App\Setting;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class LicenceExpire extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'licence-expire';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set licence expire status of companies in companies table.';

    /**
     * Create a new command instance.
`     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $companies = Company::where('status', 'active')
            ->whereNotNull('licence_expire_on')
            ->whereRaw('`licence_expire_on` < ?', [Carbon::now()->format('Y-m-d')])
            ->get();

        // Set default package for license expired companies.
        if($companies){
            $defaultPackage = Package::where('default', 'yes')->first();
            if($defaultPackage){
                foreach($companies as $company){
                    ModuleSetting::where('company_id', $company->id)->delete();

                    $moduleInPackage = (array)json_decode($defaultPackage->module_in_package);
                    $clientModules = ['projects', 'tickets', 'invoices', 'estimates', 'events', 'tasks', 'messages'];
                    if($moduleInPackage){
                        foreach ($moduleInPackage as $module) {

                            if(in_array($module, $clientModules)){
                                $moduleSetting = new ModuleSetting();
                                $moduleSetting->company_id = $company->id;
                                $moduleSetting->module_name = $module;
                                $moduleSetting->status = 'active';
                                $moduleSetting->type = 'client';
                                $moduleSetting->save();
                            }

                            $moduleSetting = new ModuleSetting();
                            $moduleSetting->company_id = $company->id;
                            $moduleSetting->module_name = $module;
                            $moduleSetting->status = 'active';
                            $moduleSetting->type = 'employee';
                            $moduleSetting->save();

                            $moduleSetting = new ModuleSetting();
                            $moduleSetting->company_id = $company->id;
                            $moduleSetting->module_name = $module;
                            $moduleSetting->status = 'active';
                            $moduleSetting->type = 'admin';
                            $moduleSetting->save();
                        }
                    }
                    $company->package_id = $defaultPackage->id;
                    $company->status = 'license_expired';
                    $company->licence_expire_on = null;
                    $company->save();
                }
            }
        }
    }
}
