<?php

namespace App\Http\Requests\SuperAdmin\Stripe;

use App\Http\Requests\SuperAdmin\SuperAdminBaseRequest;

class UpdateRequest extends SuperAdminBaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "api_key" => "required",
            "api_secret" => "required",
            "webhook_key" => "required",
        ];
    }
}
