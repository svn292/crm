<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Company;
use App\Package;
use App\StripeInvoice;
use App\Subscription;
use App\Traits\CurrencyExchange;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\File;
use App\SmtpSetting;


class SuperAdminDashboardController extends SuperAdminBaseController
{
    use CurrencyExchange;

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('app.menu.dashboard');
        $this->pageIcon = 'icon-speedometer';
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->totalCompanies = Company::all()->count();
        $this->totalPackages = Package::all()->count();
        $this->activeCompanies = Company::where('status', '=', 'active')->count();

        $this->inactiveCompanies = Company::where('status', '=', 'inactive')->count();

        $expiredCompanies =  Company::with('package')->where('status', 'license_expired')->get();
        $this->expiredCompanies = $expiredCompanies->count();;

        // Collect recent 5 licence expired companies detail
        $this->recentExpired = $expiredCompanies->sortBy('updated_at')->take(5);

        // Collect data for earning chart
        $months = [
            '1' => 'jan',
            '2' => 'Feb',
            '3' => 'Mar',
            '4' => 'Apr',
            '5' => 'May',
            '6' => 'Jun',
            '7' => 'Jul',
            '8' => 'Aug',
            '9' => 'Sep',
            '10' => 'Oct',
            '11' => 'Nov',
            '12' => 'Dec',
        ];

        $invoices = StripeInvoice::selectRaw('SUM(amount) as amount,YEAR(pay_date) as year, MONTH(pay_date) as month')->havingRaw('year = ?', [Carbon::now()->year])->groupBy('month')->get()->groupBy('month')->toArray();

        $chartData = [];
        foreach ($months as $key => $month) {
            if (key_exists($key, $invoices)) {
                foreach ($invoices[$key] as $amount) {
                    $chartData[] = ['month' => $month, 'amount' => $amount['amount']];
                }
            } else {
                $chartData[] = ['month' => $month, 'amount' => 0];
            }
        }

        $this->chartData = json_encode($chartData);

        // Collect data of recent registered 5 companies
        $this->recentRegisteredCompanies = Company::with('package')->take(5)->latest()->get();

        // Collect recent 5 subscriptions data
        $this->recentSubscriptions = Subscription::with('company', 'company.package')->take(5)->latest()->get();

        //get latest update
        $client = new Client();
        $res = $client->request('GET', config('laraupdater.update_baseurl') . '/laraupdater.json', ['verify' => false]);
        $lastVersion = $res->getBody();
        $lastVersion = json_decode($lastVersion, true);

        if ($lastVersion['version'] > File::get(public_path() . '/version.txt')) {
            $this->lastVersion = $lastVersion['version'];
        }

        //check the missing configurations
        $this->smtpSetting = SmtpSetting::first();
        $missingItems = 0;
        $totalConfigRequired = 4;
        if ($this->global->google_map_key == '') {
            $missingItems = $missingItems + 1;
        }
        if ($this->global->google_recaptcha_key == '') {
            $missingItems = $missingItems + 1;
        }
        if ($this->global->currency_converter_key == '') {
            $missingItems = $missingItems + 1;
        }
        if ($this->smtpSetting->mail_from_email == 'from@email.com') {
            $missingItems = $missingItems + 1;
        }
        $this->missingItems = $missingItems;
        $this->configCompletePercent = (100 - floor(($missingItems / $totalConfigRequired) * 100));


        return view('super-admin.dashboard.index', $this->data);
    }
}
