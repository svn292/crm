<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Company;
use App\Currency;
use App\Helper\Reply;

use App\Http\Requests\SuperAdmin\Companies\DeleteRequest;
use App\Http\Requests\SuperAdmin\Companies\StoreRequest;
use App\Http\Requests\SuperAdmin\Companies\UpdateRequest;

use App\StripeInvoice;
use App\Traits\CurrencyExchange;
use Yajra\DataTables\Facades\DataTables;

class SuperAdminInvoiceController extends SuperAdminBaseController
{
    /**
     * SuperAdminInvoiceController constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->pageTitle = 'Invoices';
        $this->pageIcon = 'icon-layers';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->totalInvoices = StripeInvoice::count();
        return view('super-admin.invoices.index', $this->data);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteRequest $request, $id)
    {
        Company::destroy($id);
        return Reply::success('Company deleted successfully.');
    }

    /**
     * @return mixed
     */
    public function data()
    {
        $packages = StripeInvoice::with(['company', 'package'])->orderBy('created_at', 'desc');

        return Datatables::of($packages)

            ->editColumn('company', function ($row) {
                    return ucfirst($row->company->company_name);
            })
            ->editColumn('package', function ($row) {
                return ucfirst($row->package->name);
            })
            ->editColumn('pay_date', function ($row) {
                return $row->pay_date->toFormattedDateString();
            })
            ->editColumn('next_pay_date', function ($row) {
                if(!is_null($row->next_pay_date)) {
                    return $row->next_pay_date->toFormattedDateString();
                }
                return '-';
            })
            ->editColumn('invoice_id', function ($row) {
                if(!is_null($row->invoice_id)) {
                    return $row->invoice_id;
                }
                return '-';
            })
            ->editColumn('transaction_id', function ($row) {
                if(!is_null($row->transaction_id)) {
                    return $row->transaction_id;
                }
                return '-';
            })
            ->make(true);
    }

}
