<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Helper\Reply;
use App\Http\Requests\SuperAdmin\Stripe\UpdateRequest;
use App\StripeSetting;


class SuperAdminStripeSettingsController extends SuperAdminBaseController
{
    public function __construct() {
        parent::__construct();
        $this->pageTitle = __('modules.stripeSettings.title');
        $this->pageIcon = 'icon-settings';
    }

    public function index() {
        $this->stripeSetting = StripeSetting::first();
        return view('super-admin.stripe-settings.index', $this->data);
    }

    public function update(UpdateRequest $request) {
        $stripe = StripeSetting::first();
        $stripe->api_key = $request->api_key;
        $stripe->api_secret = $request->api_secret;
        $stripe->webhook_key = $request->webhook_key;
        $stripe->save();

        return Reply::success(__('messages.settingsUpdated'));
    }

}
