<?php

namespace App\Http\Controllers\Admin;

use App\GlobalSetting;
use App\Http\Requests\StripePayment\PaymentRequest;
use App\Module;
use App\Package;
use App\Traits\StripeSettings;
use Illuminate\Http\Request;

class AdminBillingController extends AdminBaseController
{
    use StripeSettings;

    public function __construct() {
        parent::__construct();
        $this->pageTitle = 'Billing';
        $this->setStripConfigs();
        $this->pageIcon = 'icon-speedometer';
        $this->globalSetting = GlobalSetting::with('currency')->first();
    }

    public function index() {
        $this->nextPaymentDate = $this->company->upcomingInvoice() ? \Carbon\Carbon::createFromTimeStamp($this->company->upcomingInvoice()->next_payment_attempt)-> toFormattedDateString() : '-';

        $this->invoices = ($this->company->subscriptions->count() > 0) ? $this->company->invoices() : [];
        $this->previousPaymentDate = ($this->company->subscriptions->count() > 0) ? \Carbon\Carbon::createFromTimeStamp($this->invoices[$this->invoices->count() - 1]->date)-> toFormattedDateString() : '-';

        return view('admin.billing.index', $this->data);

    }

    public function packages() {
        $this->packages = Package::where('default', 'no')->get();
        $this->modulesData = Module::all();
        $this->pageTitle = 'Packages';
        return view('admin.billing.package', $this->data);

    }

    public function payment(PaymentRequest $request) {
        $token = $request->stripeToken;
        $email = $request->stripeEmail;
        $plan = Package::find($request->plan_id);

        if($plan->max_employees < $this->company->employees->count() ) {
            return back()->withError('You can\'t downgrade package because your employees length is '.$this->company->employees->count().' and package max employees lenght is '.$plan->max_employees)->withInput();
        }

        $company = $this->company;
        $subscription = $company->subscriptions;
        try {
            if ($subscription->count() > 0) {
                $company->subscription('main')->swap($plan->{'stripe_' . $request->type . '_plan_id'});
            }
            else {
                $company->newSubscription('main', $plan->{'stripe_'.$request->type.'_plan_id'})->create($token, [
                    'email' => $email,
                ]);
            }

            $company = $this->company;

            $company->package_id = $plan->id;
            $company->package_type = $request->type;

            // Set company status active
            $company->status = 'active';

            $company->save();

            \Session::flash('message', 'Payment successfully done.');
            return redirect(route('admin.billing'));

        } catch (\Exception $e) {
            return back()->withError($e->getMessage())->withInput();
        }
//        return back()->withError('User not found by ID ' . $request->input('user_id'))->withInput();


    }

    public function download(Request $request, $invoiceId) {
        return $this->company->downloadInvoice($invoiceId, [
            'vendor'  => $this->company->company_name,
            'product' => $this->company->package->name,
            'global' => GlobalSetting::first(),
            'logo' => $this->company->logo,
        ]);
    }
}
