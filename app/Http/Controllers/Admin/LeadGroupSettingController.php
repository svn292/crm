<?php

namespace App\Http\Controllers\Admin;

use App\Helper\Reply;
use App\Http\Requests\LeadSetting\StoreLeadSource;
use App\Http\Requests\LeadSetting\StoreRequest;
use App\Http\Requests\LeadSetting\UpdateLeadSource;
use App\Http\Requests\TicketType\UpdateTicketType;
use App\LeadGroup;
use App\LeadStatus;
use App\TicketType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LeadGroupSettingController extends AdminBaseController
{
    public function __construct() {
        parent::__construct();
        $this->pageTitle = __('app.menu.leadGroup');
        $this->pageIcon = 'ti-settings';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->leadSources = Leadgroup::all();
        return view('admin.lead-settings.group.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $source = new LeadGroup();
        $source->type = $request->type;
        $source->save();

        $allSources = LeadGroup::all();

        $select = '';
        foreach($allSources as $allSource){
            $select.= '<option value="'.$allSource->id.'">'.ucwords($allSource->type).'</option>';
        }

        return Reply::successWithData(__('messages.leadSourceAddSuccess'), ['optionData' => $select]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->source = LeadGroup::findOrFail($id);

        return view('admin.lead-settings.group.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = LeadGroup::findOrFail($id);
        $type->type = $request->type;
        $type->save();

        return Reply::success(__('messages.leadSourceUpdateSuccess'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        LeadGroup::destroy($id);

        return Reply::success(__('messages.leadSourceDeleteSuccess'));
    }

    public function createModal(){
        return view('admin.lead-settings.source.create-modal');
    }
}
