<?php

namespace App\Observers;

use App\LeadGroup;

class LeadGroupObserver
{

    public function saving(LeadGroup $source)
    {
        // Cannot put in creating, because saving is fired before creating. And we need company id for check bellow
        if (company()) {
            $source->company_id = company()->id;
        }
    }

}
